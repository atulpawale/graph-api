package com.microsoft.azuresamples.msal4j.msidentityspringbootwebapp;

// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

import com.microsoft.aad.msal4j.*;

import java.io.IOException;
import java.util.Collections;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

public class UsernamePasswordFlow {

    private  static String CLIENT_ID = "";
    private final static String AUTHORITY = "https://login.microsoftonline.com/df22b375-60fb-4005-95ee-424c774f3be3/";
    private  static String CLIENT_SECRET = "";
    private final static Set<String> SCOPE = Collections.singleton("https://graph.microsoft.com/.default");

    public static void main(String args[]) throws Exception {
        setUpSampleData();
        IAuthenticationResult result = acquireToken();
        System.out.println("Access token: " + result.accessToken());
    }

    private static IAuthenticationResult acquireToken() throws Exception {

        // This is the secret that is created in the Azure portal when registering the application

        IClientCredential credential = ClientCredentialFactory.createFromSecret(CLIENT_SECRET);
        ConfidentialClientApplication cca =
                ConfidentialClientApplication
                        .builder(CLIENT_ID, credential)
                        .authority(AUTHORITY)
                        .build();

        // Client credential requests will by default try to look for a valid token in the
        // in-memory token cache. If found, it will return this token. If a token is not found, or the
        // token is not valid, it will fall back to acquiring a token from the AAD service. Although
        // not recommended unless there is a reason for doing so, you can skip the cache lookup
        // by using .skipCache(true) in ClientCredentialParameters.
        ClientCredentialParameters parameters =
                ClientCredentialParameters
                        .builder(SCOPE)
                        .build();

        return cca.acquireToken(parameters).join();
    }

    /**
     * Helper function unique to this sample setting. In a real application these wouldn't be so hardcoded, for example
     * values such as username/password would come from the user, and different users may require different scopes
     */
    private static void setUpSampleData() throws IOException {
        // Load properties file and set properties used throughout the sample
        CLIENT_SECRET = "v7G8Q~B_EhEQ1HG9hfnd9Pi_jSiPIBN8AZV5Eax7";

        //SCOPE.add("user.read");
        CLIENT_ID = "f214131e-a776-4932-8648-bfbc46cf00a2";
        //username = "testuser@demographapipoc.onmicrosoft.com";
        //password = "@z4jrePTBT\"#*?Z";
    }
}
